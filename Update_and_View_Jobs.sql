--select @@SERVERNAME
--https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sql-server-agent-stored-procedures-transact-sql?view=sql-server-2017
USE [msdb]
GO

DECLARE
    @jobId BINARY(16)
    ,@Job_NAME nvarchar(200)
    ,@scheduleid int
	,@new_scheduleid int

SET @Job_NAME = '' --<--------PUT THE NAME OF THE JOB HERE----------<

SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = @Job_NAME)
SELECT @scheduleid = schedule_id FROM msdb.dbo.sysjobs as sj inner join [msdb].[dbo].[sysjobschedules] as js on js.job_id = sj.job_id WHERE (sj.name = @Job_NAME)

PRINT '@jobId: ' + cast(@jobId as varchar(25))
PRINT '@Name: ' + @Job_NAME
PRINT '@scheduleid: ' + cast(@scheduleid as varchar(25))

IF (@jobId IS NOT NULL)
BEGIN
	/********************GET INFORMATION STATEMENTS*************/
	--To view a specific job, either job_id or job_name must be specified. Omit both job_id and job_name to return information about all jobs.
	EXEC dbo.sp_help_job
		@job_id = @jobId
		--@job_name = @NAME

	--Either job_id or job_name must be specified, but both cannot be specified.
	--The parameters of sp_help_jobschedule can be used only in certain combinations. If schedule_id is specified, neither job_id nor job_name can be specified. Otherwise, the job_id or job_name parameters can be used with schedule_name.
	--EXEC dbo.sp_help_jobschedule
	--	@job_id = @jobId

	
	--EXEC dbo.sp_help_jobstep
	--    @job_id  = @jobId;
	/********************End of GET INFORMATION STATEMENTS******/


	/*******************UPDATE STATEMENTS********************/
	--NOTE: Either job_id or job_name must be specified but both cannot be specified.
	--EXEC dbo.sp_update_job
	--	@job_id=@jobId

		--Either job_id or job_name must be specified but both cannot be specified.
  --  EXEC msdb.dbo.sp_update_jobstep
  --      @job_id=@jobId,
		--@step_id=1, 
		--@cmdexec_success_code=0, 
		--@on_success_action=3, 
		--@on_success_step_id=0, 
		--@on_fail_action=2, 
		--@on_fail_step_id=0, 
		--@retry_attempts=3, 
		--@retry_interval=5, 
		--@os_run_priority=0, @subsystem=N'SSIS', 
		--@command=N'/FILE "C:\file.txt', 
		--@database_name=N'master', 
		--@output_file_name=N'D:\LOGS\log.log', 
		--@flags=0

	--Either job_id or job_name must be specified but both cannot be specified.
	--EXEC sp_update_jobschedule
	--EXEC dbo.sp_update_schedule  
	--	@schedule_id=@scheduleid,
 --       @freq_type=4,
 --       @freq_interval=1,
 --       @freq_subday_type=8,
 --       @freq_subday_interval=6,
 --       @active_end_time=180000


    /*******************End of UPDATE STATEMENTS********************/
	
	/********************GET INFORMATION STATEMENTS*************/
	--To view a specific job, either job_id or job_name must be specified. Omit both job_id and job_name to return information about all jobs.
	--EXEC dbo.sp_help_job
	--	@job_id = @jobId
		--@job_name = N'NightlyBackups',

	--Either job_id or job_name must be specified, but both cannot be specified.
	--The parameters of sp_help_jobschedule can be used only in certain combinations. If schedule_id is specified, neither job_id nor job_name can be specified. Otherwise, the job_id or job_name parameters can be used with schedule_name.
	--EXEC dbo.sp_help_jobschedule
	--	@job_id = @jobId

	
	--EXEC dbo.sp_help_jobstep
	--    @job_id  = @jobId;
	/********************End of GET INFORMATION STATEMENTS******/
END